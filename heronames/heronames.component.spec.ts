import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeronamesComponent } from './heronames.component';

describe('HeronamesComponent', () => {
  let component: HeronamesComponent;
  let fixture: ComponentFixture<HeronamesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeronamesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeronamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
