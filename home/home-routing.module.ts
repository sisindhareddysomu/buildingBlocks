import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentComponent}from './components/student/student.component';
import { CollegeComponent } from './components/college/college.component';
import { from } from 'rxjs';
const routes: Routes = [
  {path:'student',component:StudentComponent},
  {path:'college',component:CollegeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
