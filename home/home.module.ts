import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { StudentComponent } from './components/student/student.component';
import { CollegeComponent } from './components/college/college.component';


@NgModule({
  declarations: [StudentComponent, CollegeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule {
  
 }
