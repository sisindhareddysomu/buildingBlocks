import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HeroComponent}from './hero/hero.component';
import {HeronamesComponent}from './heronames/heronames.component' ;
import { from } from 'rxjs';

const routes: Routes = [
  {path:'hero',component:HeroComponent},
  {path:'heronames',component:HeronamesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
